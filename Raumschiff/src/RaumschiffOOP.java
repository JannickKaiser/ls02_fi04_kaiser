import java.util.ArrayList;
import java.util.Random;


public class RaumschiffOOP {

	private String schiffsName;
	private int schildeInProzent;
	private int huelleInProzent;
	private int energieversorgungInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int photonentorpedoAnzahl;
	private int androidenAnzahl;
	private ArrayList<Ladung> ladungen;
	public ArrayList<String> broadcastKommunikator;
	
	/**
	 *  Erstellt neues Raumschiff ohne Paramter
	 */
	public RaumschiffOOP() {
		
	}
	
	/**
	 * 
	 * @param schiffsName 						| Name des Schiffs
	 * @param schildeInProzent					| Gibt Zustand der Schilde an
	 * @param huelleInProzent					| Gibt Zustand der Huelle an
	 * @param energieversorgungInProzent		| Gibt Zustand der Energieversorgung an
	 * @param lebenserhaltungssystemeInProzent	| Gibt Zustand der Lebenserhaltung an
	 * @param photonentorpedoAnzahl				| Gibt Anzahl der photonentorpedos an
	 * @param androidenAnzahl					| Gibt Anzahl der Androiden an
	 */
	public RaumschiffOOP(String schiffsName, int schildeInProzent, int huelleInProzent,int energieversorgungInProzent, int lebenserhaltungssystemeInProzent, int photonentorpedoAnzahl,int androidenAnzahl) 
	{	
		this.schiffsName = schiffsName;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.androidenAnzahl = androidenAnzahl;
		this.broadcastKommunikator = new ArrayList<String>();
		this.ladungen = new ArrayList<Ladung>();
	}
	
	/**
	 * F�gt Raumschiff eine Ladung hinzu
	 * @param ladungen 
	 */
	public void addLadung(Ladung ladungen) 
	{
		this.ladungen.add(ladungen);
	}
	
	/**
	 * Gibt Auskunft �ber Schiffszustand ein
	 */
	public void zustandAusgeben() 
	{
		System.out.println("Zunstand des Schiffes: " + "[" + this.schiffsName + "]");
		System.out.println("Schilde: " + this.schildeInProzent + "%");
		System.out.println("Huelle: " + this.huelleInProzent + "%");
		System.out.println("Energieversorgung: " + this.energieversorgungInProzent + "%");
		System.out.println("Lebenserhaltung: " + this.lebenserhaltungssystemeInProzent + "%");
		System.out.println("Photonentorpedoanzahl: " + this.photonentorpedoAnzahl);
		System.out.println("Androidenanzahl: " + this.androidenAnzahl);
	}
	
	/**
	 * Gibt Ladungsverzeichnis an
	 */
	public void ladungsverzeichnisAusgeben() 
	{
		System.out.println("Ladungen von: " + "[" + this.schiffsName + "]");
		for(Ladung ladung : this.ladungen) {
			System.out.println(ladung.getBezeichnung() + " Anzahl: " + ladung.getMenge());
		}
	}
	
	/**
	 * Schie�t ein Photonentorpedo auf ein Zeil ab
	 * Falls kein Tropedo vorhanden gibt es eine Nachricht an alle 
	 * Ziel gibt an auf wen man zielt
	 * @param ziel
	 */
	public void photonentorpedosAbschie�en(RaumschiffOOP ziel) 
	{
		if (this.photonentorpedoAnzahl > 0) {
			this.photonentorpedoAnzahl--;
			this.nachrichtenAnAlle("Photonentorpedo abgeschossen");
			ziel.trefferVermerken();
		} else {
			this.nachrichtenAnAlle("-=*Click*=-");
		}	
	}
	
	/**
	 * Schie�t eine Phaserkanone ab
	 * Wenn Energieversorgung unter 50% dann gibt es eine Nachricht an alle
	 * Ziel gibt an auf wen man zielt
	 * @param ziel
	 */
	public void phaserkanoneAbschie�en(RaumschiffOOP ziel) 
	{
		if (this.energieversorgungInProzent < 50) {
			this.nachrichtenAnAlle("-=*Click*=-");
		} else {
			this.setEnergieversorgungInProzent(this.energieversorgungInProzent - 50);
			this.nachrichtenAnAlle("Phaserkanone abgeschossen");
			ziel.trefferVermerken();
		}
	}
	/**
	 * Schickt eine Nachricht an alle Crewmitglieder
	 * @param nachricht | Ist eine Nachricht
	 */
	public void nachrichtenAnAlle(String nachricht) 
	{
		this.broadcastKommunikator.add(nachricht);
		System.out.println(nachricht);
	}
	
	/**
	 * Berechnet Schaden am Schiff wenn Schiff getroffen wird
	 */
	private void trefferVermerken() 
	{
		System.out.println("[" + this.schiffsName + "]" + " wurde getroffen");
		this.setSchildeInProzent(this.schildeInProzent - 50);
		if (this.schildeInProzent == 0) {
			this.setHuelleInProzent(this.huelleInProzent - 50);
			this.setEnergieversorgungInProzent(this.energieversorgungInProzent - 50);
		if (this.huelleInProzent == 0) {
			this.setLebenserhaltungssystemeInProzent(0);
			this.nachrichtenAnAlle("Lebenserhaltungssysteme wurden vernichtet");
		}
		}
	}
	
	/**
	 * Wenn die Anzahl einer Ladung 0 ist wird es aus dem Verzeichnis gel�scht und somit aufger�umt
	 */
	public void ladungsverzeichnisAufraeumen() 
	{
		for (int i = 0; i < this.ladungen.size(); i++) 
		{
			Ladung ladungen = this.ladungen.get(i);
		 if(ladungen.getMenge() == 0) 
		 {
			 this.ladungen.remove(i);	 
		 }
		}
	}
	
	/**
	 * Gibt das Logbuch aus 
	 * @return gibt den boradcastKommunikator zur�ck
	 */
	public ArrayList<String> logbuchAusgeben() 
	{
		return this.broadcastKommunikator;
	}
	
	/**
	 * Holen Torpedos aus dem lager ins Schiff
	 * @param anzTorpedos | Gibt Anzahl der Torpedos aus
	 */
	public void photonentorpedosEinsetzen(int anzTorpedos) {
		Ladung ladung = null;
		for(Ladung l : this.ladungen) 
		{
			if(l.getBezeichnung().equalsIgnoreCase("Photonentorpedo")) {
				ladung = l;
				break;
			}
		}
		if (ladung != null) {
			if (anzTorpedos > ladung.getMenge()) {
				anzTorpedos = ladung.getMenge();
			}
			
			ladung.setMenge(ladung.getMenge() - anzTorpedos);
			this.setPhotonentorpedoAnzahl(anzTorpedos);
			System.out.println("Anzahl der eingestzen Torpedos: " + anzTorpedos);
			
		}else {
			System.out.println("Keine Photonentorpedos gefunden");
			this.nachrichtenAnAlle("-=*Click*=-");
		}
	}
	
	/**
	 * Repariert ausgew�hlte Teile des Schiffs
	 * @param schilde			| Gibt Schilde des Schiffs an
	 * @param huelle			| Gibt Huelle des Schiffs an
	 * @param energieversorgung	| Gibt Energieversorgung des Schiffs an
	 * @param lebenserhaltung	| Gibt Lebenserhaltungsysteme des Schiffs an
	 * @param anzahlAndroiden	| Gibt Anzahl der Androiden des Schiffs an
	 */
	public void reperaturAndroidenEinsetzen(boolean schilde, boolean huelle, boolean energieversorgung, boolean lebenserhaltung, int anzahlAndroiden) 
	{
		if (anzahlAndroiden > this.androidenAnzahl) {
			anzahlAndroiden = this.androidenAnzahl;
		}
		Random random = new Random();
		int anzStruktur = 0;
		if (schilde)  anzStruktur++;
		if (huelle)  anzStruktur++;
		if (energieversorgung)  anzStruktur++;
		if (lebenserhaltung)  anzStruktur++;
		
		int x = (random.nextInt(101) * anzahlAndroiden) / anzStruktur;
		
		if(schilde) 
		{
			this.setSchildeInProzent(this.schildeInProzent + x);
		}
		if(huelle) 
		{
			this.setHuelleInProzent(this.huelleInProzent + x);
		}
		if(energieversorgung) 
		{
			this.setEnergieversorgungInProzent(this.energieversorgungInProzent + x);
		}
		if(lebenserhaltung) 
		{
			this.setLebenserhaltungssystemeInProzent(this.lebenserhaltungssystemeInProzent + x);
		}
		
	}
	
	/**
	 * @return schiffsname
	 */
	public String getSchiffsName() {
		return schiffsName;
	}
	
	/**
	 * 
	 * @param schiffsName | Setzt Schiffsnamen
	 */
	public void setSchiffsName(String schiffsName) {
		this.schiffsName = schiffsName;
	}
	
	/**
	 * 
	 * @return schildeinProzent
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	
	/**
	 * 
	 * @param schildeInProzent | Setzt schildeInProzent und checkt ob es �ber 0 ist
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
		if (this.schildeInProzent < 0) {
			this.schildeInProzent = 0;
		}
	}
	
	/**
	 * 
	 * @return HuelleInProzent
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	
	/**
	 * 
	 * @param huelleInProzent | Setzt huelleInProzent und checkt ob es �ber 0 ist
	 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
		if (this.huelleInProzent < 0) {
			this.huelleInProzent = 0;
		}
	}
	
	/**
	 * 
	 * @return EnergieversorgungInProzent
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	
	/**
	 * 
	 * @param energieversorgungInProzent | Setzt energieversorgungInProzent und checkt ob es �ber 0 ist
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
		if (this.energieversorgungInProzent < 0) {
			this.energieversorgungInProzent = 0;
		}
	}
	
	/**
	 * 
	 * @return lebenserhaltungsysteme
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	
	/**
	 * 
	 * @param lebenserhaltungssystemeInProzent | Setzt lebenserhaltungsystemeInProzent und checkt ob es �ber 0 ist
	 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		if (this.lebenserhaltungssystemeInProzent < 0) {
			this.lebenserhaltungssystemeInProzent = 0;
		}
	}
	
	/**
	 * 
	 * @return photonentorpedoAnzahl
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	
	/**
	 * 
	 * @param photonentorpedoAnzahl | Setzt photnenTorpedoAnzahl und checkt ob es �ber 0 ist
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		if (this.photonentorpedoAnzahl < 0) {
			this.photonentorpedoAnzahl = 0;
		}
	}
	
	/**
	 * 
	 * @return androidenAnzahl
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	
	/**
	 * 
	 * @param androidenAnzahl | Setzt androidenAnzahl und checkt ob es �ber 0 ist
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
		if (this.androidenAnzahl < 0) {
			this.androidenAnzahl = 0;
		}
	}
	
	/**
	 * 
	 * @return broadcastKommunikator
	 */
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	
	/**
	 * 
	 * @param broadcastKommunikator | Setzt bradcastKommunikator
	 */
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	
	
	
	
	
	
	
	
	
	
	
}
