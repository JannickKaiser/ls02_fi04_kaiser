
public class Ladung {

	
		private int menge;
		private String bezeichnung;

		/**Erstellt neue Ladung aber ohne Parameter */
		public Ladung() {
			
		}
		/**
		 * Erstellt neue Ladung
		 * @param bezeichnung 	| Name der Ladung
		 * @param menge			| Menge der Ladung
		 */
		public Ladung(String bezeichnung, int menge) {
			setMenge(menge);
			this.bezeichnung = bezeichnung;
		}
		
		/**
		 * 
		 * @param menge | Setzt menge und checkt ob es �ber 0 ist
		 */
		public void setMenge(int menge) {
			if(menge > 0)
				this.menge = menge;
			else
				this.menge = 0;
		}
		
		/**
		 * 
		 * @param bezeichnung | Setzt bezeichnung 
		 */
		public void setBezeichnung(String bezeichnung) {
			 this.bezeichnung = bezeichnung;
		}
		
		/**
		 * 
		 * @return bezeichnung
		 */
		public String getBezeichnung() {
			return this.bezeichnung;
		}
		
		/**
		 * 
		 * @return menge
		 */
		public int getMenge() {
			return this.menge;
		}
}

