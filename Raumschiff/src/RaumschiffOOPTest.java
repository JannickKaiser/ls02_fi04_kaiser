


public class RaumschiffOOPTest {

    public static void main(String[] args) {
    	RaumschiffOOP klingonen = new RaumschiffOOP("IKS Hegh'ta", 100, 100, 100, 100, 1, 2);
        Ladung schneckensaft = new Ladung("Ferengi Schneckensaft", 200);
        Ladung batleth = new Ladung("Bat'leth Klingen Schwert", 200);
        
        klingonen.addLadung(schneckensaft);
        klingonen.addLadung(batleth);
        
        RaumschiffOOP romulaner = new RaumschiffOOP("IRW Khazara", 100, 100, 100, 100, 2, 2);
        Ladung borgSchrott = new Ladung("Borg-Schrott", 5);
        Ladung plasmaWaffe = new Ladung("Plasma-Waffe", 50);
        Ladung roteMaterie = new Ladung("Rote Materie", 2);
        
        romulaner.addLadung(borgSchrott);
        romulaner.addLadung(plasmaWaffe);
        romulaner.addLadung(roteMaterie);
        
        RaumschiffOOP vulkanier = new RaumschiffOOP("Ni'Var", 80, 50, 80, 100, 0, 5);
        Ladung forschungssonde = new Ladung("Forschungssonde", 35);
        Ladung photonentorpedo = new Ladung("Photonentorpedo", 3);
        
        vulkanier.addLadung(forschungssonde);
        vulkanier.addLadung(photonentorpedo);
        
        klingonen.photonentorpedosAbschießen(romulaner);
        System.out.println();
        romulaner.phaserkanoneAbschießen(klingonen);
        System.out.println();
        vulkanier.nachrichtenAnAlle("Gewalt ist nicht logisch");
        System.out.println();
        klingonen.zustandAusgeben();
        System.out.println();
        klingonen.ladungsverzeichnisAusgeben();
        System.out.println();
        vulkanier.reperaturAndroidenEinsetzen(true,true,true,false,5);
        System.out.println();
        vulkanier.photonentorpedosEinsetzen(3);
        System.out.println();
        vulkanier.ladungsverzeichnisAufraeumen();
        System.out.println();
        klingonen.photonentorpedosAbschießen(romulaner);
        klingonen.photonentorpedosAbschießen(romulaner);
        System.out.println();
        klingonen.zustandAusgeben();
        System.out.println();
        klingonen.ladungsverzeichnisAusgeben();
        System.out.println();
        romulaner.zustandAusgeben();
        System.out.println();
        romulaner.ladungsverzeichnisAusgeben();
        System.out.println();
        vulkanier.zustandAusgeben();
        System.out.println();
        vulkanier.ladungsverzeichnisAusgeben();
    }
}