package Rechteck;


public class Rechteck {

	private double x;
	private double y;
	private double width;
	private double height;
	
	
	public Rechteck(double x,double y,double width,double height)
	{
	  setX(x);
	  setY(y);
	  setWidth(width);
	  setHeight(height);
	}
	
	public void setX(double x) {
		if(x > 0)
			this.x = x;
		else
			this.x = 0;
	}
	
	public double getX() {
        return x;
    }
	
	public void setY(double y) {
		if(y > 0)
			this.y =  y;
		else
			this.y = 0;
	}
	
	public double getY() {
        return y;
    }
	
	public void setWidth(double width) {
		if(width > 0)
			this.width = width;
		else
			this.width = 0;
	}
	
	public double getWidth() {
        return width;
    }
	
	public void setHeight(double height) {
		if( height > 0)
			this.height =  height;
		else
			this.height = 0;
	}
	
	public double getHeight() {
        return height;
    }
	
	public double getDiagonale() {
		return Math.sqrt(Math.pow(this.width, 2) + Math.pow(this.height, 2));
	}
	
	public double getFlaeche() {
		return this.width * this.height;
	}
	
	public double getUmfang() {
		return this.height + this.height + this.width + this.width;
	}
}


